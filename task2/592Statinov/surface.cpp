#include "./common/Mesh.hpp"
#include "./common/Application.hpp"
#include "./common/ShaderProgram.hpp"
#include "./common/Texture.hpp"
#include "./common/LightInfo.hpp"
#include <glm/simd/geometric.h>
#include <vector>
#include <algorithm>
#include <cmath>
#include <iostream>

class SurfaceApplication :
        public Application
{
public:
    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        _marker = makeSphere(0.1f);
        _surface1 = generateSurface(-1, 0, N, 1);
        _surface1->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        _surface2 = generateSurface(0, 1, N, -1);
        _surface2->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        //=========================================================
        //Инициализация шейдеров
        _markerShader = std::make_shared<ShaderProgram>("592StatinovData2/marker.vert", "592StatinovData2/marker.frag");
        _textureShader = std::make_shared<ShaderProgram>("592StatinovData2/texture.vert", "592StatinovData2/texture.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        //=========================================================
        //Загрузка и создание текстур
        _texture1 = loadTexture("592StatinovData2/texture1.jpg");
        _texture2 = loadTexture("592StatinovData2/texture2.jpg");

        //=========================================================
        //Инициализация сэмплеров
        glGenSamplers(1, &_sampler1);
        glSamplerParameteri(_sampler1, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler1, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler1, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler1, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_sampler2);
        glSamplerParameteri(_sampler2, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler2, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler2, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler2, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("surface", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light")) {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, -10.0f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, 2.0f * glm::pi<float>());
            }
        }
        ImGui::End();
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _textureShader->use();

        _textureShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _textureShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _textureShader->setVec3Uniform("light.pos", lightPosCamSpace);
        _textureShader->setVec3Uniform("light.La", _light.ambient);
        _textureShader->setVec3Uniform("light.Ld", _light.diffuse);
        _textureShader->setVec3Uniform("light.Ls", _light.specular);

        float time = glfwGetTime();
        float diff_time = 1;
        _textureShader->setFloatUniform("time", time/3);
        _textureShader->setFloatUniform("diff", diff_time);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler1);
        _texture1->bind();

        _textureShader->setIntUniform("diffuseTex", 0);
        _textureShader->setMat4Uniform("modelMatrix", _surface1->modelMatrix());
        _textureShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _surface1->modelMatrix()))));

        _surface1->draw();

        _texture1->unbind();

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(1, _sampler2);
        _texture2->bind();

        _textureShader->setIntUniform("diffuseTex", 0);
        _textureShader->setMat4Uniform("modelMatrix", _surface2->modelMatrix());
        _textureShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _surface2->modelMatrix()))));

        _surface2->draw();

        //Рисуем маркер
        _markerShader->use();
        _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
        _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
        _marker->draw();

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(1, 0);
        glUseProgram(0);
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);
        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_MINUS ) {
                updateSurface(-2);
            } else if (key == GLFW_KEY_EQUAL) {
                updateSurface(2);
            }
        }
    }

private:
    MeshPtr generateSurface(int start, int finish, int N, int direction)
    {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> textures;

        float PI = glm::pi<float>();
        float dx = (finish - start) * PI / (float)N;
        float dV = 2 * PI /(float)N; //full circle

        for (int i = 0; i < N; ++i) {
            float curr_x = dx * i;
            float next_x = dx * (i + 1);
            for (int j = 0; j < N; ++j) {
                float curr_v = dV * j;
                float next_v = dV * (j + 1);

                float help_const = (1 - direction) / 2;
                glm::vec3 e = makePoint(curr_x + start * PI, curr_v);
                glm::vec3 f = makePoint(curr_x + start * PI, next_v);
                glm::vec3 g = makePoint(next_x + start * PI, next_v);
                glm::vec3 h = makePoint(next_x + start * PI, curr_v);

                glm::vec2 e_t = makeTextureCoord(float(j) / N, float(i) / N, direction);
                glm::vec2 f_t = makeTextureCoord(float(j) / N, float(i + 1) / N, direction);
                glm::vec2 g_t = makeTextureCoord(float(j + 1) / N, float(i + 1) / N, direction);
                glm::vec2 h_t = makeTextureCoord(float(j + 1) / N, float(i) / N, direction);

                //---------------------------

                vertices.emplace_back(e);
                vertices.emplace_back(f);
                vertices.emplace_back(g);

                vertices.emplace_back(g);
                vertices.emplace_back(h);
                vertices.emplace_back(e);

                textures.emplace_back(e_t);
                textures.emplace_back(f_t);
                textures.emplace_back(g_t);

                textures.emplace_back(g_t);
                textures.emplace_back(h_t);
                textures.emplace_back(e_t);


                glm::vec3 normal = calculateNormal(e,f,g);

                for (int k = 0; k < 6; ++k)
                    normals.emplace_back(normal);

            }

        }
        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf2->setData(textures.size() * sizeof(float) * 2, textures.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());
        return mesh;
    }

    glm::vec3 makePoint(float x, float v)
    {
        float R = sin(x);
        return glm::vec3(x, R * sin(v), R * cos(v));
    }

    glm::vec2 makeTextureCoord(float u, float v, float direction)
    {
        float help = (1 - direction) / 2;
        return glm::vec2(direction * u + help, direction * v + help);
    }

    glm::vec3 calculateNormal(glm::vec3 a, glm::vec3 b, glm::vec3 c) {
        glm::vec3 result;

        glm::vec3 vec1(b[0]-a[0], b[1]-a[1], b[2]-a[2]);
        glm::vec3 vec2(c[0]-a[0], c[1]-a[1], c[2]-a[2]);

        result[0] = vec1[1]*vec2[2] - vec1[2]*vec2[1];
        result[1] = vec1[2]*vec2[0] - vec1[0]*vec2[2];
        result[2] = vec1[0]*vec2[1] - vec1[1]*vec2[0];

        if (a[2] > 0) {
            result[0] = -1 * result[0];
            result[1] = -1 * result[1];
            result[2] = -1 * result[2];
        }

        return normalize(result);
    }

    void updateSurface(int dN)
    {
        N += dN;

        _surface1 = generateSurface(-1, 0, N, 1);
        _surface1->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        _surface2 = generateSurface(0, 1, N, -1);
        _surface2->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        draw();
    }
private:
    MeshPtr _surface1;
    MeshPtr _surface2;
    MeshPtr _marker;

    //Идентификатор шейдерной программы
    ShaderProgramPtr _markerShader;
    ShaderProgramPtr _textureShader;

    TexturePtr _texture1;
    TexturePtr _texture2;

    GLuint _sampler1;
    GLuint _sampler2;

    LightInfo _light;

    //Переменные для управления положением одного источника света
    float _lr = 10.0;
    float _phi = glm::pi<float>() * 0.25f;
    float _theta = 3.14 / 2;

    unsigned int N = 400;
};

int main() {
    SurfaceApplication app;
    app.start();
    return 0;
}
